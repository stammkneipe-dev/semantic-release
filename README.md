# Operator Packager

> A Docker Image to create a Semantic Release

## Usage

```yaml
# .gitlab-ci.yml
release_weekly:
  image: registry.gitlab.com/stammkneipe-dev/semantic-release:latest
  script:
    - git config --global --add safe.directory '*'
    - npx -p @semantic-release/changelog -p @semantic-release/exec -p @semantic-release/git -p @semantic-release/gitlab semantic-release
```

```yaml
# .releaserc.yaml
branches:
  - main
preset: angular
tagFormat: ${version}

plugins:
  - "@semantic-release/commit-analyzer"
  - "@semantic-release/release-notes-generator"
  - "@semantic-release/changelog"
  - - "@semantic-release/git"
    - message: |
        chore(release): ${nextRelease.version}
  - "@semantic-release/gitlab"
```

## Batteries Included

- [nodejs](https://github.com/curl/curl)
- [git](https://github.com/git/git)
- [yq](https://github.com/golang/go)

## Contributing

[Contributing](/CONTRIBUTING.md)

## License

[MIT License](/LICENSE)

## Authors and acknowledgment

- [Patrick Domnick](https://gitlab.com/PatrickDomnick)
- [Henry Sachs](https://gitlab.com/DerAstronaut)
