# [1.1.0](https://gitlab.com/stammkneipe-dev/semantic-release/compare/1.0.0...1.1.0) (2024-03-17)


### Features

* create tags ([492fd0b](https://gitlab.com/stammkneipe-dev/semantic-release/commit/492fd0bc049f79c2e160afe4910a3ea62e00ffa1))

# 1.0.0 (2024-03-17)


### Bug Fixes

* build ([bd8b2c0](https://gitlab.com/stammkneipe-dev/semantic-release/commit/bd8b2c0290b0d3bb67bad09b5a3ab8dd01c7b282))
* ci token ([f24307c](https://gitlab.com/stammkneipe-dev/semantic-release/commit/f24307ca1480851cd241ff8ca8a0260545397224))
* paths ([693da51](https://gitlab.com/stammkneipe-dev/semantic-release/commit/693da511492bb1699decdf92a54007b7e4c42799))
* releaser ([ead8d28](https://gitlab.com/stammkneipe-dev/semantic-release/commit/ead8d28303458ddb20a04656e80672a6334060cb))
* token ([da25469](https://gitlab.com/stammkneipe-dev/semantic-release/commit/da25469234b0c3b157aeb2325348f2928c764ab3))
* upload path ([a50255d](https://gitlab.com/stammkneipe-dev/semantic-release/commit/a50255d873c373ad5b130c8bfb29701dcff0ec4d))
